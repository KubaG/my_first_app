var path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const CompressionPlugin = require('compression-webpack-plugin')
// const BrotliPlugin = require('brotli-webpack-plugin')

module.exports = () => {

  const entry = path.resolve(__dirname, "dist") + '/index.html'

  let plugins = [
    new HtmlWebpackPlugin({
        hash: true,
        template: path.resolve(__dirname, "dist") + '/local.html',
        filename: entry
    })
  ]

  return {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: [
      require.resolve(path.resolve(__dirname) + '/polyfills'),
      './src/webapp/App.tsx'
    ],
    output: {
      publicPath: '/app',
      path: path.resolve(__dirname, "dist") + '/app',
      filename: "app.js"
    },
    resolve: {
      extensions: [".ts", ".tsx", ".js"]
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          test: /\.ts|\.tsx$/,
          loader: "ts-loader",
          exclude: [/node_modules/, /api/]
        }
      ]
    },
    stats: 'errors-only',
    plugins
  }
}
