import AppServer from "./src/shared/server"
import config from './src/config'

const appServer = new AppServer(config)

const server = appServer.start()

const sign = ['SIGINT', 'SIGTERM']

sign.forEach((sig: any) => {
  process.on(sig, () => {
    server.close()
    console.info('app_sigterm_server_killed')
    process.exit()
  })
})
