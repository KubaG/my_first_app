// Promise: https://github.com/taylorhakes/promise-polyfill
// fetch: https://github.com/developit/unfetch
// ScrollIntoView: https://github.com/KoryNunn/scroll-into-view

if (Array.prototype.find === undefined) {
  Array.prototype.find = function (fn) {
    for (var i = 0, l = this.length; i < l; i++) {
      if (fn(this[i], i, this)) {
        return this[i];
      }
    }
  }
}

if (Array.prototype.findIndex === undefined) {
  Array.prototype.findIndex = function (fn) {
    for (var i = 0, l = this.length; i < l; i++) {
      if (fn(this[i], i, this)) {
        return i;
      }
    }

    return -1;
  }
}

if (Array.prototype.includes === undefined) {
  Array.prototype.includes = function () {
    return this.indexOf(arguments) !== -1;
  }
}

if (Object.assign === undefined) {
  Object.assign = function (target) {
    for (var i = 1, l = arguments.length, o; i < l; i++) {
      o = arguments[i];

      for (var key in o) {
        target[key] = o[key];
      }
    }

    return target;
  }
}

if (Object.entries === undefined) {
  Object.entries = function (o) {
    var keys = Object.keys(o),
        l = keys.length,
        entries = new Array(l),
        i, key;

    for (i = 0; i < l; i++) {
      key = keys[i];
      entries[i] = [key, o[key]];
    }

    return entries;
  }
}

if (Object.values === undefined) {
  Object.values = function (o) {
    var keys = Object.keys(o),
        l = keys.length,
        values = new Array(l),
        i, key;

    for (i = 0; i < l; i++) {
      key = keys[i];
      values[i] = o[key];
    }

    return values;
  }
}