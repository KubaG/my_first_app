import insertExpense from './api/expenses/insert'
import { IConfig } from './shared/intf/IConfig'

const config: IConfig = {
  url: 'http://localhost',
  port: 3000,
  api: {
    routes: [
      {
        name: 'Add an expense',
        method: 'POST',
        uri: '/api/expenses/insert',
        endpoint: insertExpense
      }
    ]
  }
}

export default config
