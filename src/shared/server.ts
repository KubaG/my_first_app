import * as express from 'express'
import { Server } from 'http'
import { IConfig } from './intf/IConfig'
import { IRoute } from './intf/IRoute'
const helmet = require('helmet')
const bodyParser = require('body-parser')

class AppServer {

  private config: IConfig

  constructor(config: IConfig) {
    this.config = config
  }

  public start(callback?: any): Server {

    const statusText = `Application running: "${this.config.url}", Port: "${this.config.port}"`

    const app = express()

    app.use(helmet())

    app.use(bodyParser.urlencoded({ extended: true }))

    app.use(bodyParser.json())

    const routes = this.config.api.routes

    try {
      this.registerApiRoutes(app, routes)
      this.registerWebAppRoutes(app)
    } catch (launchError) {
      console.error('server_launch_error')
      process.exit(1)
    }

    return app.listen(this.config.port, () => {
      console.info(`Application is listening.\n${statusText}`)
      if (callback) {
        callback()
      }
    })
  }

  private registerWebAppRoutes(app: express.Express): express.Express {
    const cmdDir = process.cwd()

    app.use('/app', express.static(cmdDir + '/dist/app'))
    console.info('+ /app (static mapping)', true)

    app.use('/styles', express.static(cmdDir + '/dist/styles'))
    console.info('+ /styles (static mapping)', true)

    app.get('*', (req: any, res: any) => {
      const indexPath = `${cmdDir}/dist/index.html`
      res.sendFile(indexPath)
    })

    return app
  }

  private registerApiRoutes(app: express.Express, routes: IRoute[]): express.Express {
    if (routes.length > 0) {
      routes.forEach((route: IRoute) => {
        console.info(`+ ${route.uri}`, true)
        switch (route.method) {
          case 'GET':
            app.route(route.uri).get(route.endpoint)
            break
          case 'POST':
            app.route(route.uri).post(route.endpoint)
            break
          default:
            console.error('server_bind_unknown_request')
        }
      })
    } else {
      throw new Error('server_bind_no_routes')
    }
    return app
  }
}

export default AppServer
