import { ISemanticDropdown } from '../intf/IReact'
const categories: ISemanticDropdown[] = [
  {
    key: 1,
    text: 'Rent',
    value: 'rent'
  },
  {
    key: 2,
    text: 'Electric',
    value: 'electric'
  },
  {
    key: 3,
    text: 'Self-development',
    value: 'personal'
  },
  {
    key: 4,
    text: 'Fun',
    value: 'fun'
  }
]

export default categories