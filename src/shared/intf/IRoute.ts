export interface IRoute {
  name: string
  method: 'POST' | 'GET'
  uri: string
  endpoint: any
}
