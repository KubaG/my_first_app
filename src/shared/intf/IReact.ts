export interface ISemanticDropdown {
  key: number
  text: string
  value: string | number
}
