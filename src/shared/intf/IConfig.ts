import { IRoute } from './IRoute'

export interface IConfig {
  url: string
  port: number
  api: {
    routes: IRoute[]
  }

}