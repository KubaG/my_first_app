import * as React from 'react'
import axios from 'axios'
import { ISemanticDropdown } from '../../shared/intf/IReact'
import { Grid, Input, Dropdown, Button } from 'semantic-ui-react'

interface INewExpenseProps {
  categories: ISemanticDropdown[]
}

interface INewExpenseState {
  amount: string
  expense: string
  category: string
}

class NewExpense extends React.Component<INewExpenseProps, INewExpenseState> {

  public constructor(props: INewExpenseProps) {
    super(props)
    this.state = {
      amount: '',
      expense: '',
      category: ''
    }
  }

  public render(): JSX.Element {
    console.log(this.state)
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column textAlign="center">
            <h1>New Expense form</h1>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered={true}>
          <Grid.Column mobile={12} textAlign="center">
            <Input
              name="amount"
              icon="dollar sign"
              value={this.state.amount}
              fluid={true}
              iconPosition="left"
              onChange={this.onInputChange}
              placeholder="Type amount... i.e. 20.12$"
            />
          </Grid.Column>
          <Grid.Column mobile={12}>
            <Input
              name="expense"
              icon="pencil"
              value={this.state.expense}
              fluid={true}
              iconPosition="left"
              onChange={this.onInputChange}
              placeholder="Type expense name: new bike"
            />
          </Grid.Column>
          <Grid.Column mobile={12}>
            <Dropdown
              name="category"
              selection={true}
              value={this.state.category}
              fluid={true}
              onChange={this.onInputChange}
              options={this.props.categories}
              placeholder="Choose expense category"
            />
          </Grid.Column>
          <Grid.Column mobile={12} textAlign="center">
            <Button
              name="btAddExpense"
              primary={true}
              circular={true}
              icon="plus"
              content="Add expense"
              onClick={this.onSubmitClick}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid >
    )
  }

  private onInputChange = (e: any, d: any) => {
    const { name, value } = d
    const newState = { ...this.state }
    newState[name] = value
    this.setState(newState)
  }

  private onSubmitClick = async () => {
    let response: any
    try {
      response = await axios.post('/api/expenses/insert', this.state)
    } catch (e) {
      response = e
    }
  }


}

export default NewExpense
