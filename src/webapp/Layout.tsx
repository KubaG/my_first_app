import * as React from 'react'

import { Grid } from 'semantic-ui-react'

const Layout = (props: any): JSX.Element => {
  return (
    <div className="App" id="app">
      <Grid>
        <Grid.Column mobile={14} textAlign="center">>
          <nav className="App__Nav">
            <ul>
              <li><a href="#">Income</a></li>
              <li><a href="#">Expense</a></li>
              <li><a href="#">Summary</a></li>
            </ul>
          </nav>
        </Grid.Column>
        <Grid.Column mobile={16}>
          {props.children}
        </Grid.Column>
      </Grid>
    </div>
  )
}

export default Layout
