import * as React from 'react'
import * as ReactDOM from 'react-dom'

import Layout from './Layout'
import NewExpense from './containers/NewExpense'

import categories from '../shared/props/categories'

ReactDOM.render(
  <Layout>
    <NewExpense
      categories={categories}
    />
  </Layout>,
  document.getElementById('app') as HTMLElement
)


