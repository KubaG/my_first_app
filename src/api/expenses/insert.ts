export default async (req: any, res: any) => {
  const data = {
    status: 200,
    data: {
      inserted: true,
      expense: req.body.expense
    }
  }
  return res.status(200).json(data)
}
